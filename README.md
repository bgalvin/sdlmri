## SDLMRI 

#### Overview

This is a rough Matlab implementation of split dictionary learning for MRI that makes use of YAML config files and some parallelization in the OMP step.

#### Setup 

Tested on Matlab 2012a, there are no dependencies on outside packages.  If your system can make use of Matlab's worker pool it will be used.

#### Use 

To run from command line: ```./run_sdlmri <your_config_file>  ```  
To run in matlab:  ```loadSpec( '<your_config_file>' ) ```

