

figure
imagesc(rebuilt_BW)
title('our result')

figure
imagesc(rebuilt_TW)
title('rebuilt TW using dictTW')

figure
imagesc(rebuilt_TW_incol)
title('rebuilt TW after atomization/deatomization')

figure
imagesc(rebuilt_BW_incol)
title('rebuilt BW after atomization/deatomization')

figure
imagesc((rebuilt_BW_incol+rebuilt_TW_incol)/2)
title('strangecomb')