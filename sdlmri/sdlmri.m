function [ results, runStuff ] = sdlmri( params )
%SDLMRI(params) - the main sdlmri call, invoked by loadSpec.m

try
    matlabpool close force local;
catch
end
try
    myCluster = parcluster('local');
    matlabpool(myCluster.NumWorkers);
catch
    exit;
end
dbstop if error

addpath('atomGen','display','sparseApprox','utils')

timestamp = now;
[~, matName, ~] = fileparts(params.study.matFile);
fileLoc = strcat(params.fileinfo.pathstr,'/',params.fileinfo.name,'_',matName,'_',num2str(timestamp));
mkdir(fileLoc);
copyfile(params.fileinfo.fullpath,fileLoc)

tic


%pull atomizer fwd and bck functions from param files.
fprintf('Finding atomizer functions... \n')
atomizer   = str2func(params.atom.typeFwd);
deatomizer = str2func(params.atom.typeBck);


%atom gen
fprintf('Atomizing data (TW)... \n')
[runStuff.TW_incol, params.atom]= atomizer(params.study.tw, params.atom);
fprintf('done! TW atom matrix is size: (%d, %d, %d) \n', size(runStuff.TW_incol))
fprintf('Atomizing data (BW)... \n')
[runStuff.BW_incol, params.atom]= atomizer(params.study.bw, params.atom);
fprintf('done! BW atom matrix is size: (%d, %d, %d) \n', size(runStuff.BW_incol))


%norm and remove max
if params.ksvd.centerData
    
    runStuff.kspace_nav_pmeans = (ones(size(runStuff.TW_incol(:,1),1),1)*mean(runStuff.TW_incol));
    runStuff.kspace_avg_pmeans = (ones(size(runStuff.BW_incol(:,1),1),1)*mean(runStuff.BW_incol));
    
    runStuff.TW_incol = runStuff.TW_incol - runStuff.kspace_nav_pmeans;
    runStuff.BW_incol = runStuff.BW_incol - runStuff.kspace_avg_pmeans;
    
    runStuff.stdNav = ones(size(runStuff.TW_incol(:,1),1),1)*std(runStuff.TW_incol);
    runStuff.stdAvg = ones(size(runStuff.TW_incol(:,1),1),1)*std(runStuff.BW_incol);
    
    runStuff.TW_incol = runStuff.TW_incol./runStuff.stdNav;
    runStuff.BW_incol = runStuff.BW_incol./runStuff.stdAvg;
    
else
    
    runStuff.kspace_nav_pmeans = zeros(size(runStuff.TW_incol));
    runStuff.kspace_avg_pmeans = zeros(size(runStuff.TW_incol));
    
    runStuff.TW_incol = runStuff.TW_incol - runStuff.kspace_nav_pmeans;
    runStuff.BW_incol = runStuff.BW_incol - runStuff.kspace_avg_pmeans;
    
    runStuff.stdNav = ones(size(runStuff.TW_incol));
    runStuff.stdAvg = ones(size(runStuff.TW_incol));
    
    runStuff.TW_incol = runStuff.TW_incol./runStuff.stdNav;
    runStuff.BW_incol = runStuff.BW_incol./runStuff.stdAvg;
    
end


%runStuff.TW_incol = [ runStuff.TW_incol; -mean(runStuff.TW_incol)*size(runStuff.TW_incol,1)];
%runStuff.BW_incol = [ runStuff.BW_incol; -mean(runStuff.BW_incol)*size(runStuff.BW_incol,1)];

%Dictionary = Dictionary*diag(1./sqrt(sum((conj(Dictionary)).*Dictionary)));


%runStuff.TW_incol = [runStuff.TW_incol  - runStuff.BW_incol ];

runStuff.combinedSignals = [runStuff.TW_incol ; runStuff.BW_incol  ];

rng(2)
%create dict for ksvd
fprintf('Initializing dictionary... \n')
toUseIdx = randperm(size(runStuff.combinedSignals,2));
params.ksvd.initialDictionary = runStuff.combinedSignals(:,toUseIdx(1:params.ksvd.dictSize));

ksvdOMPopts = [];
ksvdOMPopts.slowMode = 0;
ksvdOMPopts.printEvery = 0;
ksvdOMPopts.ignoreCorrupted = 0;
ksvdOMPopts.runCHOL = 0;


%prep for feed to ksvd
fprintf('Sending to KSVD... \n')
[runStuff.dict_COMB, runStuff.opts]  = ksvd_wrapper(runStuff.combinedSignals, params.ksvd ,ksvdOMPopts);
%gam_TW = opts.CoefMatrix;


%build using dict
fompopts.slowMode = 0;
fompopts.printEvery = 0;
fompopts.ignoreCorrupted = 0;
fompopts.runCHOL = 0;

fprintf('Rebuilding BW using TW dict... \n')
%runStuff.gam_BW = wrapFOMP( runStuff.dict_COMB, runStuff.BW_incol, params.ksvd.constraint.numAtoms, [], fompopts );
fprintf('Rebuilding TW using TW dict... \n')
runStuff.gam_COMB = wrapFOMP( runStuff.dict_COMB, runStuff.combinedSignals, params.ksvd.constraint.numAtoms, [], fompopts );

runStuff.dict_onlyTW = runStuff.dict_COMB(1:size(runStuff.BW_incol,1),:);
runStuff.dict_onlyBW = runStuff.dict_COMB((size(runStuff.BW_incol,1)+1):size(runStuff.TW_incol,1)*2,:);



%rebuild images, convert from cols to SOS kspace
fprintf('De-atomizing data (TW)... \n')
results.rebuilt_TW = deatomizer( runStuff.dict_onlyTW*runStuff.gam_COMB.*runStuff.stdNav + runStuff.kspace_nav_pmeans, params.atom );
fprintf('De-atomizing data (BW)... \n')
results.rebuilt_BW = deatomizer( runStuff.dict_onlyBW*runStuff.gam_COMB.*runStuff.stdAvg + runStuff.kspace_avg_pmeans, params.atom );



%rebuild images directly from orig data for comparison
fprintf('De-atomizing data (TW)... \n')
results.rebuilt_TW_incol = deatomizer( runStuff.TW_incol.*runStuff.stdNav+runStuff.kspace_nav_pmeans, params.atom );
fprintf('De-atomizing data (BW)... \n')
results.rebuilt_BW_incol = deatomizer( runStuff.BW_incol.*runStuff.stdAvg+runStuff.kspace_avg_pmeans, params.atom );

results.orig_tw = params.study.tw;
results.orig_bw = params.study.bw;

results.avgPmeans = deatomizer(runStuff.kspace_avg_pmeans , params.atom);
results.navPmeans = deatomizer(runStuff.kspace_nav_pmeans , params.atom);
results.avgPstd = deatomizer(runStuff.stdAvg , params.atom);
results.navPstd = deatomizer(runStuff.stdNav , params.atom);

%dict_AVG = rows_AVG*gNAVNAV'/(gNAVNAV*gNAVNAV');
runStuff.gam_BWBW = wrapFOMP( runStuff.dict_onlyBW , runStuff.BW_incol, params.ksvd.constraint.numAtoms, [], fompopts );
results.rebuilt_TWBWBW = deatomizer( runStuff.dict_onlyTW*runStuff.gam_BWBW.*runStuff.stdAvg + runStuff.kspace_avg_pmeans , params.atom);
results.rebuilt_BWBWBW = deatomizer( runStuff.dict_onlyBW*runStuff.gam_BWBW.*runStuff.stdAvg + runStuff.kspace_avg_pmeans , params.atom);

%results.rebuilt_TWBWBW = deatomizer( runStuff.dict_onlyTW*runStuff.gam_BWBW, params.atom);
%results.rebuilt_BWBWBW = deatomizer( runStuff.dict_onlyBW*runStuff.gam_BWBW, params.atom);
%results.rebuilt_DIFFpAVG = deatomizer( runStuff.dict_onlyBW*runStuff.gam_BWBW + runStuff.dict_onlyTW*runStuff.gam_BWBW, params.atom);


%save results
results.comptime = toc;

%matLoc = strcat(params.fileinfo.pathstr,'/results_',num2str(now),'.mat');

fprintf('Saving results to: %s... \n', fileLoc)
save( strcat(fileLoc,'/runStuff_',num2str(timestamp),'.mat'),'runStuff','-v7.3');
save( strcat(fileLoc,'/results_',num2str(timestamp),'.mat'),'results','-v7.3');

end

