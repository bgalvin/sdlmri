function [ snrout1 ] = mySNR( inMat1,slize, xFG, yFG, xBG, yBG  )

if length(size(inMat1)) == 3
    FGmean1 = mean2(abs(inMat1(yFG(1):yFG(2),xFG(1):xFG(2),slize)));
    BGmean1 = mean2(abs(inMat1(yBG(1):yBG(2),xBG(1):xBG(2),slize)));
    BGstd1  =  std2(abs(inMat1(yBG(1):yBG(2),xBG(1):xBG(2),slize)));
else
    FGmean1 = mean2(abs(inMat1(yFG(1):yFG(2),xFG(1):xFG(2))));
    BGmean1 = mean2(abs(inMat1(yBG(1):yBG(2),xBG(1):xBG(2))));
    BGstd1  =  std2(abs(inMat1(yBG(1):yBG(2),xBG(1):xBG(2))));
end

snrout1 = abs(FGmean1)/BGstd1;

end

