close all
rng(1)

meansn = []
meansa = []
for i=1:2:16
    
    a = 1;
    b = size(dict_AVG,2);
    r = round((b-a).*rand(1) + a);
    
    colAVG = dict_AVG(:,r);
    colNAV = dict_NAV(:,r);
    recolAVG = reshape(colAVG,[8 6 6]);
    recolNAV = reshape(colNAV,[8 6 6]);
    
    subplot(8,2,0+i)
    imagesc(abs(ifftshift(ifftn(recolNAV(:,:,4)))));
    
    meansn = [meansn mean2(abs(ifftshift(ifftn(recolNAV(:,:,:)))))/std2(abs(ifftshift(ifftn(recolNAV(:,:,:)))))];
    
    
    ylabel(num2str(r))
    set(gca,...
        'XTickLabel','')
    set(gca,...
        'YTickLabel','')
    
    if i==1
        title('Nav')
    end
    
    subplot(8,2,1+i)
    imagesc(abs(ifftshift(ifftn(recolAVG(:,:,4)))));
    
    meansa = [meansa mean2(abs(ifftshift(ifftn(recolAVG(:,:,:)))))/std2(abs(ifftshift(ifftn(recolAVG(:,:,:)))))];
    
    set(gca,...
        'XTickLabel','')
    set(gca,...
        'YTickLabel','')
    
    
    if i==1
        title('Avg')
    end
    
    pause(.1)
    
end

compiledMeans = [meansn ;meansa]
mean(compiledMeans')

%%

out = [0 ; 0];
for i=1:1:size(dict_AVG,2)
    
    colAVG = dict_AVG(:,i);
    colNAV = dict_NAV(:,i);
    recolAVG = reshape(colAVG,[8 6 6]);
    recolNAV = reshape(colNAV,[8 6 6]);
    
    rea = (abs(ifftshift(ifftn(recolAVG))));
    ren = (abs(ifftshift(ifftn(recolNAV))));
    
    snra = mean2(rea)/std2(rea);
    snrn = mean2(ren)/std2(ren);
    
    out(:,end+1)=[ snra;snrn];
    
end


%%

