function [thissnr ] = displaySlice( dispMat , slicetodisp ,xFG, yFG, xBG, yBG,aMax,aMin, thistitle)

figure
thistitle = strrep(thistitle,'_','\_')
thissnr  = mySNR(dispMat, slicetodisp, xFG, yFG, xBG, yBG);

if length(size(dispMat)) > 2
    imagesc(abs(dispMat(:,:,slicetodisp)));
    %imagesc(rot90( rot90(abs(dispMat(:,:,slicetodisp)))));
    title( strcat(thistitle,' - SNR: ', num2str(thissnr)))
    caxis([aMin,aMax ])
    
else
    %imagesc(abs(dispMat(80:200,70:200)));
    imagesc(abs(dispMat()));
    title( strcat(thistitle,' - SNR: ', num2str(thissnr)))
    caxis([aMin,aMax ])
end

colormap(gray)
brighten(.6)

pause(0.00001);
frame_h = get(handle(gcf),'JavaFrame');
set(frame_h,'Maximized',1);




end

