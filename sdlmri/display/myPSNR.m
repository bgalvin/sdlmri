function [ psnr mse ] = myPSNR( truth,test )
%MYPSNR Summary of this function goes here
%   Detailed explanation goes here
mse= sum((abs(test(:))-abs(truth(:))).^2)/length(test(:));
psnr = 10*log10(max(abs(test(:)))^2/mse);


end

