function [ dict_TW, opts ] = ksvd_wrapper(inputMat, params ,ksvdOMPopts)
%KSVD_WRAPPER Summary of this function goes here
%   Detailed explanation goes here

ksvdParams.K = params.dictSize;
ksvdParams.numIteration = params.niter;
ksvdParams.preserveDCAtom = params.preserveDCatom;

ksvdParams.errorFlag = params.constraint.type;

if ksvdParams.errorFlag == 0
    ksvdParams.L = params.constraint.numAtoms;
else
    ksvdParams.errorGoal = params.constraint.errorThres;
end

ksvdParams.InitializationMethod = 'GivenMatrix';
ksvdParams.initialDictionary = params.initialDictionary;
ksvdParams.displayProgress = 1;

ksvdParams

[dict_TW, opts] = KSVDC2(inputMat, ksvdParams,ksvdOMPopts, params.constraint.errorThres);

end

