clear all
close all 

rng(5)
addpath('utils')

% x = randn(10000,1);
% %sig(2) = 0
% D = randn(10000,2000);

% sigLength = 100;
% numSigs = 1000;
% dsize = 1000;
% numCoefs = 65;


sigLength = 100;
numSigs = 1;
dsize = 200;
numCoefs = 100;

x = complex(randn(sigLength,numSigs),randn(sigLength,numSigs));
D = complex(randn(sigLength,dsize),randn(sigLength,dsize));
D = D*diag(1./sqrt(sum((conj(D)).*D)));
% x = randn(sigLength,numSigs);
% D = randn(sigLength,dsize);
% D = normcols(D);

% x = real(x);
% D = real(D);


% tic
% Dgram = D'*D;
% for i = 1:1:numSigs
% DTx = D'*x(:,i);
% A1=ompdMIL(x(:,i),D,Dgram,DTx,numCoefs,1e-16);
% end
% MIL = toc


opts.slowMode  = 0;
opts.ignoreCorrupted=0;
opts.runCHOL = 1;

tic
A2=wrapFOMP(D,x,numCoefs,[],opts);
CHOL = toc

% tic
% Dgram = D'*D;
% for i = 1:1:numSigs
% DTx = D'*x(:,i);
% A3=ompdQR(x(:,i),D,Dgram,DTx,numCoefs,.1e-16);
% end
% QR = toc

opts.slowMode  = 0;
opts.ignoreCorrupted=0;
opts.runCHOL = 0;

tic
A4=wrapFOMP(D,x,numCoefs,[],opts);
fomp = toc

% tic
% for i = 1:1:numSigs
% A5=OMP(D,x(:,i),numCoefs);
% end
% naive = toc


%A1=ompdChol(x,D,D'*D,D'*x,3,.001);
% D*A 
% x
% Ar=wrapFOMP(real(D),real(x),3,[],opts);
% Ai=wrapFOMP(imag(D),imag(x),3,[],opts);

%norm(x-D*A1)
norm(x-D*A2)
norm(x-D*A4)
%norm(D*A5-x)


% tic
% B=OMP(D,x,3);
% toc
% tic
% C = ompdChol(x, D, D'*D, D'*x, 3, 1e-16);
% toc

% OUTPUTS
% s  - solution to x = dict*s

% sigoutA = D*A
% sigoutB= D*B
% sigoutC= D*C
% norm(sigoutA(:) - sigoutB(:))

% opts.printEvery = 0;
% opts.maxiter = 10;
% opts.slowMode = 0 ;
% %   'opts'  is a structure with more options, including:
% %       .printEvery = is an integer which controls how often output is printed
% %       .maxiter    = maximum number of iterations
% %       .slowMode   
% [A]=wrapFOMP(D,sig,7,[],opts);



%%


clear all
close all 

rng(5)
addpath('utils')

% x = randn(10000,1);
% %sig(2) = 0
% D = randn(10000,2000);

% sigLength = 100;
% numSigs = 1000;
% dsize = 1000;
% numCoefs = 65;


sigLength = 8;
numSigs = 1;
dsize = 20;
numCoefs = 2;

x = complex(randn(sigLength,numSigs),randn(sigLength,numSigs));
D1 = complex(randn(sigLength,dsize),randn(sigLength,dsize));
D1 = D1*diag(1./sqrt(sum((conj(D1)).*D1)));
D2 = complex(randn(sigLength,dsize),randn(sigLength,dsize));
D2 = D2*diag(1./sqrt(sum((conj(D2)).*D2)));


ps = [2 2 2]
sigSize = size(D1(:,1))
for i=1:1:size(D1,2)

    Dres = reshape(D1(:,i),[2 2 2])
   
    yps(:,1,1) = exp(  (1:1:ps(1)).*(1i*randn(1)) );
    xps(1,:,1) = exp(  (1:1:ps(2)).*(1i*randn(1)) );
    zps(1,1,:) = exp(  (1:1:ps(3)).*(1i*randn(1)) );
    ypsmat = repmat(yps,[1,ps(2),ps(3)]);
    xpsmat = repmat(xps,[ps(1),1,ps(3)]);
    zpsmat = repmat(zps,[ps(1),ps(2),1]);
    DresShift = Dres.*ypsmat.*xpsmat.*zpsmat ;
    
    D1(:,i) = reshape(DresShift,[8 1])
    
end



opts.slowMode  = 0;
opts.ignoreCorrupted=0;
opts.runCHOL = 1;

tic
A2=wrapFOMP(D1,x,numCoefs,[],opts);
CHOL = toc



D3 =(A2*A2')'*x*A2'


%%
clear all
close all

addpath(genpath('/home/sci/bgalvin/projects/dict_learning/sdlmri'))
addpath('/home/sci/bgalvin/projects/dict_learning/sdlmri/examples/coupled/config_patches_MID227_735923.5083')

%load results_735923.5083.mat
%load runStuff_735923.5083.mat


sigLength = 10;
numSigs = 1;
dsize = 20;
numCoefs = 4;

x = complex(randn(sigLength,numSigs),randn(sigLength,numSigs));
D = complex(randn(sigLength,dsize),randn(sigLength,dsize));
D = D*diag(1./sqrt(sum((conj(D)).*D)));


D = runStuff.dict.full;
D = D*diag(1./sqrt(sum((conj(D)).*D)));
x = runStuff.BW_incol.full(:,2);


mask = rand(size(x))>.4;

x_masked = x.*mask

opts.mask = mask;
opts.slowMode  = 0;
opts.ignoreCorrupted=1;
opts.runCHOL = 0;

A4=wrapFOMP(D,x_masked,numCoefs,[],opts);
a1 = OMP(D(mask,:),x(mask,:),4)


[x x_masked  D*a1]
[x(mask,:) D(mask,:)*a1]

norm(x-D*A4)




%%


clear all
close all

addpath(genpath('/home/sci/bgalvin/projects/dict_learning/sdlmri'))

img = phantom;
params.pshape = [4 4];
params.sshape = [1 1];
X = real(complex_im2col(img,params));
imagesc(X)

D = X(:,randperm(size(X,2),50))


mask = rand(size(x))>.4;

x_masked = x.*mask

opts.mask = mask;
opts.slowMode  = 0;
opts.ignoreCorrupted=1;
opts.runCHOL = 0;

A4=wrapFOMP(D,x_masked,numCoefs,[],opts);
a1 = OMP(D(mask,:),x(mask,:),4)


[x x_masked  D*a1]
[x(mask,:) D(mask,:)*a1]

norm(x-D*A4)









