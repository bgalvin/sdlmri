function [] = loadSpec( yamlStruct )
%LOADSPEC(configFile) Accepts a .yaml config file, parses and checks for errors

%goto debug on error 
dbstop if error 
%make sure we have yaml proc. included
addpath('yaml')

%read the yaml file 
params = ReadYaml(yamlStruct);

%store some info about the config file for I/o later
[params.fileinfo.pathstr,params.fileinfo.name,params.fileinfo.ext] = fileparts(yamlStruct);
params.fileinfo.fullpath = yamlStruct;

if ~isfield(params,'study') || ~isfield(params,'ksvd') || ~isfield(params,'atom')
    error('you done goofed! ~ config file is missing an somthing important')
end

% err checking goes here

fprintf('Loading config file from: %s... \n', yamlStruct)

if isfield(params.study,'matFile')
    fprintf('Found .MAT file, loading... \n')
    load(strcat(params.fileinfo.pathstr,'/',params.study.matFile))
    params.study.tw = eval(params.study.trainingData);
    params.study.bw = eval(params.study.buildingData);
end

params.study.outputDir = params.fileinfo.pathstr;

% pass to mainfunc
fprintf('Looks good so far passing to SDLMRI... \n')

sdlmri(params);

end

