%close all
clear all
addpath('utils\','sparseApprox\','snrAndDisplay\')

%filenames = 'MID281_2d_final_nonorm_means_nc2_TW_kspace_nav_BW_kspace_avg_ds_500_ps_4_L_10';
filenames = 'MID227_resized_2d_final_nonorm_means_nc2_TW_kspace_nav_BW_kspace_avg_ds_500_ps_4_L_10';
firstsplit = strsplit(filenames,'_nc');
secondSplit =  strsplit(firstsplit{2},'_TW');



for nc = [1 2 3 4 5]
    
    fname = strcat('../results/',...
        firstsplit{1},...
        '_nc',...
        num2str(nc),...
        '_TW',...
        secondSplit{2});
    
    load(fname)
    
    if nc == 1
        toAdd = {recon_NAV,recon_AVG,...
            recon_dAVG_gNAVNAV,recon_dNAV_gNAVAVG,recon_dNAV_gNAVNAV...
            };
        toAdd = cellfun(@(x){abs(x).^2},toAdd);
    else
        toAdd2 = {recon_NAV,recon_AVG,...
            recon_dAVG_gNAVNAV,recon_dNAV_gNAVAVG,recon_dNAV_gNAVNAV...
            };
        toAdd = cellfun(@(x,y){x + abs(y).^2},toAdd, toAdd2);
    end
    
end

toAdd = cellfun(@(x){sqrt(x)},toAdd);
names = {'recon_clean','recon_mov',...
    'recon_dAVG_gNAVNAV','recon_dNAV_gNAVAVG','recon_dNAV_gNAVNAV'};



for i=1:1:length(toAdd)
    for ii=1:size(toAdd{i},3)
        toAdd{i}(:,:,ii)=rot90(toAdd{i}(:,:,ii),2);
    end
end

if usejava('jvm') && ~feature('ShowFigureWindows')
else
    %slice 6 has a nice blood vessel on the right side that moves between
    %avg and nav
    chooseSlice  = 10;
    
    displayMulitiple(toAdd,names, chooseSlice)
    
    ssimMat = [];
    diffMat = []
    for i = 1:size(toAdd,2)
        for j = 1:size(toAdd,2)
            diffMat(i,j) = norm(toAdd{i}(:)-toAdd{j}(:));
            ssimMat(i,j) = get_ssim(toAdd{i},toAdd{j},chooseSlice );
        end
    end
    
end

