#!/bin/bash
# cmd line starter for matlab sdlmri.  first input parameter should be the config.yaml file.


#screen -S $1 -d -m sh -c 'matlab -nosplash -nodisplay -r "loadSpec('$1'), quit()"'

FULLPATH=$(readlink -f $1)

matlab -nosplash -nodisplay -r "loadSpec_coupled('$FULLPATH'), quit()"
#echo "started $1 on new screen"

#sleep 1

