function [ results, runStuff ] = coupled_kspace_dl( params )
%SDLMRI(params) - the main sdlmri call, invoked by loadSpec.m

try
    matlabpool close force local;
catch
end
try
    myCluster = parcluster('local');
    matlabpool(myCluster.NumWorkers);
catch
    exit;
end
dbstop if error

addpath('atomGen','display','sparseApprox','utils')

timestamp = now;
[~, matName, ~] = fileparts(params.study.matFile1);
fileLoc = strcat(params.fileinfo.pathstr,'/',params.fileinfo.name,'_',matName,'_',num2str(timestamp));
mkdir(fileLoc);
copyfile(params.fileinfo.fullpath,fileLoc)

rng(5)
tic


%pull atomizer fwd and bck functions from param files.
fprintf('Finding atomizer functions... \n')
atomizer   = str2func(params.atom.typeFwd);
deatomizer = str2func(params.atom.typeBck);

params.atom.mat1size = size(params.study.tw);
params.atom.mat2size = size(params.study.bw);

%params.atom.minDimensions = [ min([ size(params.study.tw,1) size(params.study.bw,1)]), min([ size(params.study.tw,2) size(params.study.bw,2)]) ];


%atom gen
fprintf('Atomizing data (TW)... \n')
[runStuff.TW_incol.full , runStuff.TW_incol.cat , params.atom]= atomizer(params.study.tw, params.atom);
fprintf('done! TW atom matrix is size: (%d, %d, %d) \n', size(runStuff.TW_incol.full))
fprintf('Atomizing data (BW)... \n')
[runStuff.BW_incol.full , runStuff.BW_incol.cat params.atom]= atomizer(params.study.bw, params.atom);
fprintf('done! BW atom matrix is size: (%d, %d, %d) \n', size(runStuff.BW_incol.full))

params.atom.cropped = 0;
results.orig_TW.full = deatomizer( runStuff.TW_incol.full, params.atom );
results.orig_BW.full = deatomizer( runStuff.BW_incol.full, params.atom );

params.atom.cropped = 1;
results.orig_TW.cat  = deatomizer( runStuff.TW_incol.cat , params.atom );
results.orig_BW.cat  = deatomizer( runStuff.BW_incol.cat , params.atom );


runStuff.combinedSignals = [runStuff.TW_incol.full ; runStuff.TW_incol.cat  ];


%create dict for ksvd
fprintf('Initializing dictionary... \n')
%toUseIdx = randperm(size(runStuff.combinedSignals,2));
toUseIdx = 26:round(size(runStuff.combinedSignals,2)/40):size(runStuff.combinedSignals,2);
params.ksvd.initialDictionary = [runStuff.combinedSignals(:,1:25) runStuff.combinedSignals(:,toUseIdx)];
params.ksvd.dictSize = size(params.ksvd.initialDictionary,2)
%params.ksvd.initialDictionary = [runStuff.combinedSignals(:,1:25) runStuff.combinedSignals(:,toUseIdx(1:params.ksvd.dictSize))];
%params.ksvd.initialDictionary = runStuff.combinedSignals;



ksvdOMPopts = [];
ksvdOMPopts.slowMode = 0;
ksvdOMPopts.printEvery = 0;
ksvdOMPopts.ignoreCorrupted = 0;
ksvdOMPopts.runCHOL = 0;


%prep for feed to ksvd
fprintf('Sending to KSVD... \n')
[runStuff.dict_COMB, runStuff.opts]  = ksvd_wrapper(runStuff.combinedSignals, params.ksvd ,ksvdOMPopts);
%gam_TW = opts.CoefMatrix;



%OVERWRITE TRAINED DICT!!!
dict_COMB = runStuff.combinedSignals;

%build using dict
fompopts.slowMode = 0;
fompopts.printEvery = 0;
fompopts.ignoreCorrupted = 0;
fompopts.runCHOL = 0;

fprintf('Finding combined alpha using combined dict... \n')
runStuff.gam_COMB = wrapFOMP( runStuff.dict_COMB, runStuff.combinedSignals, round(params.ksvd.constraint.numAtoms), [], fompopts );


fprintf('Splitting dictionary into sub-components... \n')

%norm the new dictionarys
runStuff.dict.full   = runStuff.dict_COMB( 1:size(runStuff.BW_incol.full,1)      ,:);
runStuff.dict.cat    = runStuff.dict_COMB( (size(runStuff.BW_incol.full,1)+1):end,:);

params.atom.cropped = 0;
results.rebuilt_TW_COMB.full = deatomizer( runStuff.dict.full*runStuff.gam_COMB, params.atom );
params.atom.cropped = 1;
results.rebuilt_TW_COMB.cat  = deatomizer( runStuff.dict.cat *runStuff.gam_COMB, params.atom );

% make the dictionary rotationally invariant in a hacky way
if params.ksvd.rotInvar
    runStuff.rotInvarDict.full = [];
    runStuff.rotInvarDict.cat = [];
	% for a smaller dict
	%for i = 1:params.atom.underSampleFactor:size( runStuff.dict.full,1)
    for i = 1:1:size( runStuff.dict.full,1)
        runStuff.rotInvarDict.full = [runStuff.rotInvarDict.full circshift(runStuff.dict.full,i)];
    end
    
    %for i = 1:size( runStuff.dict.cat,1)
    %    runStuff.rotInvarDict.cat = [runStuff.rotInvarDict.cat circshift(runStuff.dict.cat,i)];
    %end
    
    runStuff.dict.full = runStuff.rotInvarDict.full;
    runStuff.dict.cat =  runStuff.rotInvarDict.full(1:params.atom.underSampleFactor:end,:); 
end


% the new dicts need to norm to 1 column wise
runStuff.dict.full = runStuff.dict.full*diag(1./sqrt(sum((conj(runStuff.dict.full)).*runStuff.dict.full)));
runStuff.dict.cat  = runStuff.dict.cat *diag(1./sqrt(sum((conj(runStuff.dict.cat )).*runStuff.dict.cat )));

fprintf('Rebuild training cat and full separately... \n')
runStuff.gam_TW.full = wrapFOMP( runStuff.dict.full,  runStuff.TW_incol.full , params.ksvd.constraint.numAtoms, [], fompopts );
params.atom.cropped = 0;
results.rebuilt_TW.full = deatomizer( runStuff.dict.full*runStuff.gam_TW.full, params.atom );
fprintf('Rebuilding cat training data with cat dictionary... \n')
runStuff.gam_TW.cat = wrapFOMP( runStuff.dict.cat,  runStuff.TW_incol.cat , round(params.ksvd.constraint.numAtoms), [], fompopts );
params.atom.cropped = 1;
results.rebuilt_TW.cat = deatomizer( runStuff.dict.cat*runStuff.gam_TW.cat, params.atom );
fprintf('Rebuilding cat trainign data with full dictionary... \n')
params.atom.cropped = 0;
results.rebuilt_TW.cat_with_full_dict = deatomizer( runStuff.dict.full*runStuff.gam_TW.cat, params.atom );

%circshift(abs(runStuff.dict.full*runStuff.gam_BW.cat),2)

fprintf('Rebuilding full test data with full dictionary... \n')
runStuff.gam_BW.full = wrapFOMP( runStuff.dict.full,  runStuff.BW_incol.full , params.ksvd.constraint.numAtoms, [], fompopts );
params.atom.cropped = 0;
results.rebuilt_BW.full = deatomizer( runStuff.dict.full*runStuff.gam_BW.full, params.atom );
fprintf('Rebuilding cat test data with cat dictionary... \n')
runStuff.gam_BW.cat = wrapFOMP( runStuff.dict.cat,  runStuff.BW_incol.cat , round(params.ksvd.constraint.numAtoms), [], fompopts );
params.atom.cropped = 1;
results.rebuilt_BW.cat = deatomizer( runStuff.dict.cat*runStuff.gam_BW.cat, params.atom );
fprintf('Rebuilding cat test data with full dictionary... \n')
params.atom.cropped = 0;
results.rebuilt_BW.cat_with_full_dict = deatomizer( runStuff.dict.full*runStuff.gam_BW.cat, params.atom );


results.params = params;

%save results
results.comptime = toc;

%matLoc = strcat(params.fileinfo.pathstr,'/results_',num2str(now),'.mat');

fprintf('Saving results to: %s... \n', fileLoc)
save( strcat(fileLoc,'/runStuff_',num2str(timestamp),'.mat'),'runStuff','-v7.3');
save( strcat(fileLoc,'/results_',num2str(timestamp),'.mat'),'results','-v7.3');

end

