function [ output_args, inCols_cropped, params] = rays2halfRings(input_args,params )
% Summary of this function goes here
%   Detailed explanation goes here

output_args = input_args.';

params.inColsSize = size(output_args);
inCols_cropped = output_args(1:params.underSampleFactor:end,:);

end

