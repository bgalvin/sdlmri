function  [toRet, params]  = cols2sos(bigSpace, params)


sos = zeros(params.imSize);

for i = 1:size(bigSpace,2)/params.inColsSize(2)
    
    curCoilInRow = bigSpace(:,(i-1)*params.inColsSize(2)+1:i*params.inColsSize(2));
    %inOrigSize = complex_col2im( curCoilInRow, params.imSize, params.pshape, params.sshape);
    
    inOrigSize = genImgFromRadPatch_3d_func(curCoilInRow, params);
    
    if params.center
        inOrigSize = phase_shift(inOrigSize,params.imSize, params.diffs{i});
    end
    
    recon = fftshift(ifftn(fftshift(inOrigSize)));
    sos = sos+abs(recon).^2;
    
end

toRet = sqrt(sos);

end

