function [inCols_toReturn, inCols_cropped_toReturn, params]= sosKspace2rays(inputMat,params)
%KSPACE2RAYS Summary of this function goes here
%   Detailed explanation goes here


if iscell(inputMat)
    inCols_cropped_toReturn=[];
    inCols_toReturn = [];
    params.nc = size(inputMat,2);
    
    for i = 1:size(inputMat,2)
        
        if params.center
            inputMat{i} = center_kspace(inputMat{i});
        end
        
        %minDim = params.minDimensions;
        params.imsize = size(inputMat{i});
        
        %dim1 = round((imsize(1)-minDim(1))/2)+1:round((imsize(1)-minDim(1))/2) +(minDim(1));
        %dim2 = round((imsize(2)-minDim(2))/2)+1:round((imsize(2)-minDim(2))/2) +(minDim(2));
        
        [inCols, params]= genRadPatch_func(inputMat{i},params);
        params.inColsSize = size(inCols);
		
		inCols = inCols.';
		
        inCols_toReturn = [inCols_toReturn inCols];
        inCols_cropped_toReturn = [inCols_cropped_toReturn inCols(1:params.underSampleFactor:end,:)];
		
		params.inColsSizeFull = size(inCols_toReturn);
        
    end
else
    
    if params.center
        inputMat = center_kspace(inputMat);
    end
    
    %minDim = params.minDimensions;
    params.imsize = size(inputMat);
    
    %dim1 = round((imsize(1)-minDim(1))/2)+1:round((imsize(1)-minDim(1))/2) +(minDim(1));
    %dim2 = round((imsize(2)-minDim(2))/2)+1:round((imsize(2)-minDim(2))/2) +(minDim(2));
    
    [inCols, params]= genRadPatch_func(inputMat,params);
    
    inCols_toReturn = inCols.';
    params.inColsSize = size(inCols_toReturn);
    inCols_cropped_toReturn = inCols_toReturn(1:params.underSampleFactor:end,:);
end
end

