function [ output, params ] = genImgFromRadPatch_func(inCols, params )
%GENIMGFROMRADPATCH_FUNC Summary of this function goes here
%   Detailed explanation goes here

%r = [-min(params.imSize)/2:params.radiusIncrement:min(params.imSize)/2];
r = [0:params.radiusIncrement:min(params.imSize)/2];
theta = [0:params.thetaIncrement:359]*pi/180;

if params.cropped
	%r = r(1:2:end);
end


[tPol, rPol] = meshgrid(theta,r);
[xPol,yPol] = pol2cart(tPol(:),rPol(:));

centered_yPol = yPol+params.imSize(1)/2;
centered_xPol = xPol+params.imSize(2)/2;

F_grid = [];

for i = 1:size(inCols,2)
    F_grid = cat(1,F_grid, inCols(:,i));
end

warning('off','all')
[x, y ] = meshgrid(1:params.imSize(2), 1:params.imSize(1));
vq = griddata(centered_xPol, centered_yPol, F_grid,x,y,'cubic');
output = vq;
output(isnan(output))=0+0i;
warning('on','all')

end

