function [inCols, inCols_cropped,params]= kspace2patch(inputMat,params)
%KSPACE2RAYS Summary of this function goes here
%   Detailed explanation goes here

minDim = params.minDimensions;
imsize = size(inputMat);

dim1 = round((imsize(1)-minDim(1))/2)+1:round((imsize(1)-minDim(1))/2) +(minDim(1));
dim2 = round((imsize(2)-minDim(2))/2)+1:round((imsize(2)-minDim(2))/2) +(minDim(2));

%[inCols, params]= genRadPatch_func(inputMat(dim1,dim2),params);

[inCols, params]= complex_im2col(inputMat(dim1,dim2),params);
params.inColsSize = size(inCols);
inCols_cropped = inCols(1:params.cropLength,:);
end

