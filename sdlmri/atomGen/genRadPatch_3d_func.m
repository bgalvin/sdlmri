function [inCols, params]= genRadPatch_3d_func(inputMat,params)
%genRadPatch_func Summary of this function goes here
%   Detailed explanation goes here

%pad for interp

%image = padarray(inputMat,[padsize padsize],'replicate');

params.imSize = size(inputMat);

% dont need all datapoints but is easier this way
%[Xcart Ycart] = ind2sub(size(image),[1:params.imSize(1)*params.imSize(2)]);
%minorAxis = find(params.imSize == min(params.imSize));
%majorAxis = find(params.imSize == max(params.imSize));

% spec desired samples in polar coords
inCols = [];

theta=((0:params.thetaIncrement:359)*pi/180);
r = [0:params.radiusIncrement:max(params.imSize)/2];
phi = ((-90:params.thetaIncrement:90)*pi/180);
 
%fprintf('%d/%d% \n',theta*(180/pi),phi*(180/pi));


%[tPol, phiPol, rPol] =meshgrid(theta,phi,r);
[ tPol,rPol, phiPol] = meshgrid(theta(:),r(:),phi(:));

% convert desired samps into cartesian and center
%[xPol,yPol,zPol] = sph2cart(tPol(:),phiPol(:),rPol(:));
[xPol,yPol,zPol] = sph2cart(tPol(:),phiPol(:),rPol(:));

centered_yPol = yPol+params.imSize(1)/2;
centered_xPol = xPol+params.imSize(2)/2;
centered_zPol = zPol+params.imSize(3)/2;

F_grid = interp3(inputMat, centered_xPol, centered_yPol,centered_zPol,'cubic',0+0i);

for  i = 1:length(r):length(theta)*length(r)*length(phi)
%scatter3(centered_yPol(i:i+length(r)-1),centered_xPol(i:i+length(r)-1),centered_zPol(i:i+length(r)-1),'*')
%axis([0 imsize(1) 0 imsize(2) 0 imsize(3)])
%pause(.000001)
inCols(:,end+1) = F_grid(i:i+length(r)-1);
end

params.inColsSize = size(inCols);

end
