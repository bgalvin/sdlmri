function [ backInRays ] = rings2rays( inRings )
%RINGS2RAYS Summary of this function goes here
%   Detailed explanation goes here

[sx, sy ] = size(inRings);
backInRays = complex(zeros([sy*2 sx/2]),zeros([sy*2 sx/2]));
colCounter = 1;
for i = 1:sy-1
    oneCol = inRings(:,i);
    backInRays(colCounter+1,:) = oneCol(1:sx/2);
    backInRays(sy*2-colCounter+1,:) = oneCol(sx/2+1:end);
    colCounter = colCounter +1;
    
    
%     figure(2)
%     imagesc(abs(backInRays))
%     
end
oneCol = inRings(:,i+1);
backInRays(1,:) = oneCol(1:sx/2);
backInRays(sy*2-colCounter+1,:) = oneCol(sx/2+1:end);

% figure(2)
% imagesc(abs(backInRays))

end

