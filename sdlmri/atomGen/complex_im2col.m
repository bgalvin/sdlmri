function [ output ,params ] = complex_im2col( inputMat, params)
% complex_im2col(matrix, [n1 n2 n3], [m1 m2 m3] ))
% n - block size
% m - step size

ks_real = real(inputMat);
ks_imag = imag(inputMat);

params.rsize = size(inputMat);

%YAML makes cell matrices so convert these to vectors
if iscell(params.pshape)
pshape = cell2mat( params.pshape);
sshape = cell2mat( params.sshape);
RC = im2colstep(ks_real, pshape, sshape);
IC = im2colstep(ks_imag, pshape, sshape);
else

RC = im2colstep(ks_real, params.pshape, params.sshape);
IC = im2colstep(ks_imag, params.pshape, params.sshape);
end

output = complex(RC,IC);

end

