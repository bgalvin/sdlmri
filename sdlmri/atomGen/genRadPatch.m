clear all
%close all
clc

load('../../data/data.mat','kspace_nav','kspace_avg');

image_orig = fftshift(ifftn(fftshift(kspace_avg)));
image_orig = fftshift(fftn(fftshift(image_orig(:,:,10))));

image_orig = center_kspace(image_orig);

recon = fftshift(ifftn(fftshift(image_orig)));

%image_orig = randn(imxsize,imysize)
%image_orig = mat2gray(imread('cameraman.tif'));


%%%%%%% start

%get image size
im_orig_size = size(image_orig);

%pad for interp
padsize = 0;
image = padarray(image_orig,[padsize padsize],'replicate');
imsize = size(image);

% dont need all datapoints but is easier this way
%[Xcart Ycart] = ind2sub(size(image),[1:imsize(1)*imsize(2)]);
minorAxis = find(imsize == min(imsize));
majorAxis = find(imsize == max(imsize));

% spec desired samples in polar coords
r = [0:4:min(imsize-padsize)/2];
theta = [0:13:359]*pi/180;
[tPol, rPol] =meshgrid(theta,r);


% convert desired samps into cartesian and center (polar points go neg in
% cart)
[xPol,yPol] = pol2cart(tPol(:),rPol(:));
centered_yPol = yPol+imsize(1)/2;
centered_xPol = xPol+imsize(2)/2;
F_grid = interp2(image, centered_xPol, centered_yPol,'linear');


%take it back
F_scat = scatteredInterpolant(centered_xPol, centered_yPol, F_grid,'natural','none');
[x, y ] = meshgrid(1:imsize(2), 1:imsize(1));
out_img = F_scat(x,y);
out_img_crop = out_img(padsize+1:im_orig_size(1), padsize+1:im_orig_size(2));
out_img_crop(isnan(out_img_crop))=0+0i;
%out_img_crop = out_img;

descalefactor = @(x) sign(x).*x.^(1/2);

out_img_crop_todisp = descalefactor(out_img_crop);
image_orig_todisp = descalefactor(image_orig);

figure
imagesc(abs(recon));
colormap(gray)
brighten(.5)
title('Original Image')
axis tight
export_fig('im_orig.pdf', '-a1', '-pdf','-q100','-painters');


figure
imagesc(abs(out_img_crop_todisp),[min(min(min(abs(image_orig_todisp)))) max(max(max(abs(image_orig_todisp))))])
title('Reconstructed Kspace')
axis tight
export_fig('ks_samped.pdf', '-a1', '-pdf','-q100','-painters');



figure
imagesc(abs(image_orig_todisp),[min(min(min(abs(image_orig_todisp)))) max(max(max(abs(image_orig_todisp))))])
title('Original Kspace')


export_fig('ks_orig.pdf', '-a1', '-pdf','-q100','-painters');




figure
scatter(centered_xPol,centered_yPol,5,'.')
title('Polar Sampling Pattern')

export_fig('ks_samp_pattern.pdf', '-a1', '-pdf','-q100','-painters');



figure
recon_interpd = fftshift(ifftn(fftshift(out_img_crop)));
imagesc(abs(recon_interpd),[min(min(min(abs(recon)))) max(max(max(abs(recon))))]);
colormap(gray)
title('Reconstructed Image')
brighten(.5)
export_fig('im_samped.pdf', '-a1', '-pdf','-q100','-painters');



%figure
%imagesc(abs(out_img_crop)-abs(image_orig))



