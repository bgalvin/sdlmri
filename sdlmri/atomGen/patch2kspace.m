function [inCols,params]= patch2kspace(inputMat,params)
%KSPACE2RAYS Summary of this function goes here
%   Detailed explanation goes here

if params.center 
    inputMat = center_kspace(inputMat);
end

if size(inputMat,1) < params.inColsSize(1)
inputMat(params.cropLength+1:params.inColsSize(1),:) = 0;
end

%[inCols, params]= genImgFromRadPatch_func(inputMat,params);
[inCols, params]= complex_col2im(inputMat,params);


end

