function [toRet ,params]= rays2sosKspace(inputMatFull,params)
%KSPACE2RAYS Summary of this function goes here
%   Detailed explanation goes here

toRet = [];

if size(inputMatFull,2) >= params.inColsSizeFull(2)
    
    for i = 1:params.nc
        %params.oneimsize(2):(params.inColsSize(2)-params.oneimsize(2))
        inputMat  = inputMatFull(: , 1+params.inColsSize(2)*(i-1):params.inColsSize(2)*(i) );
        
        
        if size(inputMat,1) < params.inColsSizeFull(1)
            %inputMat(params.cropLength+1:params.inColsSize(1),:) = 0;
            
            kronVec = [1 zeros(1,params.underSampleFactor-1)];
            inputMat_up =  kron(inputMat.', kronVec).';
            inputMat_up_trim = inputMat_up(1:params.inColsSize(1),:);
            inputMat_up_trim = inputMat_up_trim.';
            [inCols, params]= genImgFromRadPatch_func(inputMat_up_trim,params);
        else
            inputMat = inputMat.';
            [inCols, params]= genImgFromRadPatch_func(inputMat,params);
        end
        toRet{i} = inCols;
        
    end
    
else
	inputMat = inputMatFull;
    if size(inputMat,1) < params.inColsSize(1)
        %inputMat(params.cropLength+1:params.inColsSize(1),:) = 0;
        
        kronVec = [1 zeros(1,params.underSampleFactor-1)];
        inputMat_up =  kron(inputMat.', kronVec)';
        inputMat_up_trim = inputMat_up(1:params.inColsSize(1),:);
        inputMat_up_trim = inputMat_up_trim.';
        [inCols, params]= genImgFromRadPatch_func(inputMat_up_trim,params);
    else
        inputMat = inputMat.';
        [inCols, params]= genImgFromRadPatch_func(inputMat,params);
    end
    toRet = inCols;
    
end
end




