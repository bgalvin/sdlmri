function [ output, params ] = complex_col2im( inputMat, params  )
% complex_im2col(matrix, [s1 s2 s3], [n1 n2 n3], [m1 m2 m3] ))
% s - orig matrix size
% n - block size
% m - step size

outRC_clean = real(inputMat);
outIC_clean = imag(inputMat);

if iscell(params.pshape)
pshape = cell2mat( params.pshape);
sshape = cell2mat( params.sshape);
reoutRC_clean = col2imstep(outRC_clean, params.rsize, pshape, sshape);
reoutIC_clean = col2imstep(outIC_clean, params.rsize, pshape, sshape);
else
    
reoutRC_clean = col2imstep(outRC_clean, params.rsize, params.pshape, params.sshape);
reoutIC_clean = col2imstep(outIC_clean, params.rsize, params.pshape, params.sshape);
end

cnt = countcover(params.rsize, pshape, sshape);

reoutRC_clean = reoutRC_clean./cnt;
reoutIC_clean = reoutIC_clean./cnt;

output = complex(reoutRC_clean, reoutIC_clean);

end

