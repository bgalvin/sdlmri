function [inCols,params]= rays2kspace_noTranspose(inputMat,params)
%KSPACE2RAYS Summary of this function goes here
%   Detailed explanation goes here



if size(inputMat,1) < params.inColsSize(1)  
    kronVec = [1 zeros(1,params.underSampleFactor-1)];
    inputMat_up =  kron(inputMat.', kronVec).';
    inputMat_up_trim = inputMat_up(1:params.inColsSize(1),:);
    [inCols, params]= genImgFromRadPatch_func(inputMat_up_trim,params);
else
    [inCols, params]= genImgFromRadPatch_func(inputMat,params);
end


end

