clear all
close all

load('meas_MID170_CV_Radial7Off_triple_adeno_FID1220_Kspace');
%kSpace = zeros(256,72,4,15,2);
inRays =double(kSpace(:,:,5,15,1));
%inRays = Coil1(:,:,1)



rings = rays2halfRings(inRays);
figure;imagesc(abs(rings))
title('rings')
ds_rings = rings(1:3:72,:)
[all_data, all_mask]=rad_to_cart_bilinear_sparse_selected_rays(inRays,1,1:3:72);
       

backInRays = halfRings2rays( rings );
figure;imagesc(abs(backInRays))
title('back to rays')

[inRays_kspace, ~]    =rad_to_cart_bilinear_sparse(inRays,1);
[backInRays_kspace, ~]=rad_to_cart_bilinear_sparse(backInRays,1);

recon1 = fftshift(ifftn(fftshift(inRays_kspace)));
recon2 = fftshift(ifftn(fftshift(backInRays_kspace)));
recon3 = fftshift(ifftn(fftshift(all_data)));

figure;
imagesc(abs(recon1))
figure
imagesc(abs(recon2))
figure
imagesc(abs(recon3))







