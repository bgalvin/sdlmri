function [ output, params ] = genImgFromRadPatch_3d_func(inCols, params )
%GENIMGFROMRADPATCH_FUNC Summary of this function goes here
%   Detailed explanation goes here

theta=((0:params.thetaIncrement:359)*pi/180);
r = [0:params.radiusIncrement:max(params.imSize)/2];
phi = ((-90:params.thetaIncrement:90)*pi/180);

[ tPol,rPol, phiPol] = meshgrid(theta(:),r(:),phi(:));

[xPol,yPol,zPol] = sph2cart(tPol(:),phiPol(:),rPol(:));

centered_zPol = zPol+params.imSize(3)/2;
centered_yPol = yPol+params.imSize(1)/2;
centered_xPol = xPol+params.imSize(2)/2;

F_grid = [];

for i = 1:size(inCols,2)
    F_grid = cat(1, F_grid, inCols(:,i));
end

%supress warnings from griddata
warning('off','all')

[x, y, z ] = meshgrid(1:params.imSize(2), 1:params.imSize(1), 1:params.imSize(3));
vq = griddata(centered_xPol, centered_yPol, centered_zPol, F_grid,x,y,z);
output = vq;
output(isnan(output))=0+0i;

warning('on','all')

end

