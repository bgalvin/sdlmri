function [ inRings ] = rays2rings( inRays )
%RAYS2RINGS Summary of this function goes here
%   Detailed explanation goes here


[sx, sy ] = size(inRays);
inRings = complex(zeros([sy*2  sx/2 ]),zeros([sy*2  sx/2 ]));
%if mod(sx,2) == 0
colCounter = 1;
for i = 1:sx/2-1
    inRings(:,colCounter) = transpose([  inRays(i+1,:)  inRays(sx-i+1,:) ]) ;
    colCounter = colCounter +1;
    

    
end
inRings(:,colCounter) = transpose([  inRays(1,:)  inRays(129,:) ]) ;

end

