function [ toRet,params ] = coils2cols( input,params )
%COILS2COLS Summary of this function goes here
%   Detailed explanation goes here

toRet = [];

for nc = 1:5
    
    currentCoil = input{nc};
    
    if params.center
        [currentCoil,diffNav] = center_kspace(currentCoil);
        params.diffs{nc} = diffNav;
    end
    
    [coilInCols params] = genRadPatch_3d_func(currentCoil,params);
    toRet = [toRet coilInCols];
    
end


end

