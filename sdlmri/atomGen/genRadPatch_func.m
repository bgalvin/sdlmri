function [inCols, params]= genRadPatch_func(inputMat,params)
%genRadPatch_func Summary of this function goes here
%   Detailed explanation goes here

%pad for interp

%image = padarray(inputMat,[padsize padsize],'replicate');

params.imSize = size(inputMat);

% dont need all datapoints but is easier this way
%[Xcart Ycart] = ind2sub(size(image),[1:params.imSize(1)*params.imSize(2)]);
minorAxis = find(params.imSize == min(params.imSize));
majorAxis = find(params.imSize == max(params.imSize));

% spec desired samples in polar coords
inCols = [];

for theta=((0:params.thetaIncrement:359)*pi/180);
    %theta = 0*pi/180
    %r = [-min(params.imSize)/2:params.radiusIncrement:min(params.imSize)/2];
    r = [0:params.radiusIncrement:min(params.imSize)/2];
    
    [tPol, rPol] =meshgrid(theta,r);
    
    % convert desired samps into cartesian and center
    [xPol,yPol] = pol2cart(tPol(:),rPol(:));
    centered_yPol = yPol+params.imSize(1)/2;
    centered_xPol = xPol+params.imSize(2)/2;
    F_grid = interp2(inputMat, centered_xPol, centered_yPol,'cubic',0+0i);
    
    inCols(:,end+1) = F_grid;
    
    
end

params.inColsSize = size(inCols);

end
