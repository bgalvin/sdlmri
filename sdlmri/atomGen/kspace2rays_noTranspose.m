function [inCols, inCols_cropped,params]= kspace2rays_noTranspose(inputMat,params)
%KSPACE2RAYS Summary of this function goes here
%   Detailed explanation goes here

if params.center
    inputMat = center_kspace(inputMat);
end

imsize = size(inputMat);
[inCols, params]= genRadPatch_func(inputMat,params);

params.inColsSize = size(inCols);
inCols_cropped = inCols(1:params.underSampleFactor:end,:);

end

