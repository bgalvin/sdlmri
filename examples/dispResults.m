clear all
close all
addpath(genpath('SLphan/'),genpath('MID227/'),genpath('MID281/'),genpath('coupled/'))

addpath(genpath('/home/sci/bgalvin/projects/dict_learning/sdlmri/examples/'))
%undersampledRun need to get rid of BWBWBW
% resName = 'results_735916.5899.mat';
% runName= 'runStuff_735916.5899.mat';

%for rays not working
% resName = 'results_735923.4838.mat';
% runName= 'runStuff_735923.4838.mat';

%for no KSVD training
% resName = 'results_735923.5557.mat';
% runName= 'runStuff_735923.5557.mat';

%for individually normed dicts
% resName = 'results_735923.5744.mat';
% runName= 'runStuff_735923.5744.mat';

%for centered kspaces and ind normed dicts
resName = 'results_735924.6212.mat';
runName= 'runStuff_735924.6212.mat';

%for patches
%runName= 'runStuff_735923.5083.mat';

load(resName)
load(runName)


%toAdd = {results.rebuilt_TW_incol, results.rebuilt_TW, results.rebuilt_BW_incol, results.rebuilt_BW,...
%results.rebuilt_BWBWBW, results.rebuilt_TWBWBW };
%namest = {'Orig DIFF','Rebuilt DIFF comb. dict.','Orig AVG','Rebuilt AVG with comb. dict.','Rebuilt AVG with AVG dict','rebuilt DIFF with AVG dict'};


toAdd = {results.orig_TW.full,...
    results.orig_TW.cat,...
    results.orig_BW.full,...
    results.orig_BW.cat...
    results.rebuilt_TW.full,...
    results.rebuilt_TW.cat,...
    results.rebuilt_BW.full,...
    results.rebuilt_BW.cat,...
    results.rebuilt_BW.cat_with_full_dict };

namest = {'Orig training full','Orig training cat','Orig testing full','Orig testing cat','Training full from Comb.','Training cat from Comb','Testing full from full comb'...
    ,'Testing cat from Cat dict','Testing Cat from Full Dict.'}



toAdd = cellfun(@(x) fftshift(ifftn(fftshift(x))),toAdd,'UniformOutput',0);
toAdd = cellfun(@abs,toAdd,'UniformOutput',0);
mins  = cellfun(@min,cellfun(@min,toAdd,'UniformOutput',0),'UniformOutput',0);
maxes = cellfun(@max,cellfun(@max,toAdd,'UniformOutput',0),'UniformOutput',0);

viewInfo = 0;
if viewInfo
    figure
    imagesc(abs(runStuff.dict_COMB));
    figure
    imagesc(abs(runStuff.dict.cat));
    figure
    imagesc(abs(runStuff.dict.full));
    figure
    imagesc(abs(runStuff.gam_COMB));
    figure
    imagesc(abs(runStuff.gam_TW.full));
    figure
    imagesc(abs(runStuff.gam_TW.cat));
    figure
    imagesc(abs(runStuff.gam_COMB));
    figure
    imagesc(abs(runStuff.gam_BW.full));
    figure
    imagesc(abs(runStuff.gam_BW.cat));
    figure
    imagesc(abs(fftshift(ifftn(fftshift(results.rebuilt_TW_COMB.full)))));
    figure
    imagesc(abs(fftshift(ifftn(fftshift(results.rebuilt_TW.cat_with_full_dict)))));
    figure
    imagesc(abs(fftshift(ifftn(fftshift(results.orig_TW.cat)))));
    figure
    imagesc(abs(fftshift(ifftn(fftshift(results.rebuilt_TW.cat)))));
end

subplot = @(m,n,p) subtightplot (m, n, p, [0.05 0.01], [0.05 0.05], [0.01 0.01]);

subplot(2,3,1)
imagesc(abs( toAdd{3} ))
colormap(gray)
%brighten(.5)
title('Full orig')
axis off

subplot(2,3,2)
imagesc(abs( toAdd{7} ))
colormap(gray)
%brighten(.5)
title('Full  rebuilt with full dict.')
axis off

subplot(2,3,3)
imagesc(abs(  toAdd{9} ))
colormap(gray)
%brighten(.5)
title('Concat. rebuilt with full dict.')
axis off

subplot(2,3,4)
imagesc(abs( toAdd{4} ))
colormap(gray)
%brighten(.5)
title('Orig concat.')
axis off

subplot(2,3,5)
imagesc(( toAdd{8}  ))
title('Concat. rebuilt with concat. dict.')
brighten(.5)

axis off

subplot(2,3,6)
%imagesc((  ))
%title('Difference (AVG/rebuilt AVG)')
%brighten(.5)
axis off





% figure
% imagesc(abs(runStuff.BW_incol.cat))
% figure
% imagesc(abs(runStuff.BW_incol.full))
% figure
% imagesc(abs(runStuff.dict.cat*runStuff.gam_BW.cat))
%


% if usejava('jvm') && ~feature('ShowFigureWindows')
% else
%     %slice 6 has a nice blood vessel on the right side that moves between
%     %avg and nav
%     chooseSlice  = 10;
%
%
%     displayMulitiple(toAdd,namest, chooseSlice)
%
%     ssimMat = [];
%     diffMat = [];
%     for i = 1:size(toAdd,2)
%         for j = 1:size(toAdd,2)
%             diffMat(i,j) = norm(toAdd{i}(:)-toAdd{j}(:));
%             ssimMat(i,j) = get_ssim(toAdd{i},toAdd{j},chooseSlice );
%         end
%     end
%
%
% end

% suptitle('Dict Size = 25, \alpha = 1');

%  figure
%  imagesc(abs(results.rebuilt_DIFFpAVG))
%  colormap(gray)
%

%imagesc(abs(runStuff.combinedSignals))
% figure;
% imagesc(abs(complex_col2im(runStuff.dict_onlyBW*runStuff.gam_BWBW,[65 65],[4 4],[1 1])))
% title('Elastic Net AVG \alpha with AVG. dictionary')
% figure;
% imagesc(abs(complex_col2im(runStuff.dict_onlyTW*runStuff.gam_BWBW,[65 65],[4 4],[1 1])))
% title('Elastic Net AVG \alpha with NAV. dictionary')
% figure
% imagesc(abs(results.navPmeans))
% figure
% imagesc(abs(results.avgPmeans))
%imagesc(abs(recon_AVG-recon_dNAV_gNAVAVG_avg_pmeans))


%
% figure;
% imagesc(abs( results.orig_tw ))
% colormap(gray)
% brighten(.5)
% title('Orig NAV')
%
% figure;
%  rebuilt_NAVpDIFF = results.rebuilt_BWBWBW + results.rebuilt_TWBWBW;
% imagesc(abs(rebuilt_NAVpDIFF))
% colormap(gray)
% brighten(.5)
% title('Rebuilt NAV')
%
%
% num_sig = 5;
% figure
% plot(runStuff.gam_COMB(:,num_sig),'b')
% hold on
% plot(runStuff.gam_BWBW(:,num_sig),'r')
% legend('Combined dict \alpha','Only AVG dict \alpha')

